import os
import shutil
import numpy as np
import random

mag = '40X'
# root_src_dir = os.path.join(mag, 'SOB')
root_dst_dir = os.path.join('patient_split')
root_src_dir = os.path.join('..','BreaKHis_v1', 'histology_slides', 'breast')
# if not os.path.exists(root_dst_dir):
#     os.makedirs(root_dst_dir)

test_percent = 0.3

for type_name in ['benign', 'malignant']:
    type_dir = os.path.join(root_src_dir, type_name, 'SOB')

    train_dest_dir = os.path.join(root_dst_dir, mag, 'train', type_name)
    test_dest_dir = os.path.join(root_dst_dir, mag, 'test', type_name)
    assert(not os.path.exists(train_dest_dir))
    os.makedirs(train_dest_dir)

    assert(not os.path.exists(test_dest_dir))
    os.makedirs(test_dest_dir)

    for patient in os.listdir(type_dir):
        patient_dir = os.path.join(type_dir, patient)
        patients = os.listdir(patient_dir)
        random.shuffle(patients)
        num_patients = len(patients)
        test_patients = patients[:int(test_percent * num_patients)]
        train_patients = patients[int(test_percent * num_patients):]

        for train_patient in train_patients:
            patient_path = os.path.join(patient_dir, train_patient, mag)
            for file_name in os.listdir(patient_path):
                shutil.copy(os.path.join(patient_path, file_name), train_dest_dir)

        for test_patient in test_patients:
            patient_path = os.path.join(patient_dir, test_patient, mag)
            for file_name in os.listdir(patient_path):
                shutil.copy(os.path.join(patient_path, file_name), test_dest_dir)




    # print(len(files))

    # if not os.path.exists(root_dst_dir):
    #     os.makedirs(root_dst_dir)


#
#
# for folder in os.listdir(root_src_dir):
#     print(folder)
#     subdir = os.path.join(root_src_dir, folder)
#     assert(not os.path.isfile(subdir))
#     for dir in os.listdir(subdir):
#         path = os.path.join(subdir, dir)
#         print(path)
#         for mag in os.listdir(path):
#             mag_path = os.path.join(path, mag)
#             print(mag_path)
#             for image in os.listdir(mag_path):
#                 image_path = os.path.join(mag_path, image)
#                 dest_path = os.path.join(root_dst_dir, mag)
#                 if not os.path.exists(dest_path):
#                     os.makedirs(dest_path)
#                 assert(not os.path.isfile(os.path.join(dest_path, image)))
#                 shutil.move(image_path, dest_path)
#
#             assert (not os.listdir(mag_path))
#             os.rmdir(mag_path)
#             # exit(1)
