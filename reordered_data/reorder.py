import os
import shutil

type = 'malignant'
root_src_dir = os.path.join(type, 'SOB')
root_dst_dir = os.path.join('test')

for folder in os.listdir(root_src_dir):
    print(folder)
    subdir = os.path.join(root_src_dir, folder)
    assert(not os.path.isfile(subdir))
    for dir in os.listdir(subdir):
        path = os.path.join(subdir, dir)
        print(path)
        for mag in os.listdir(path):
            mag_path = os.path.join(path, mag)
            print(mag_path)
            for image in os.listdir(mag_path):
                image_path = os.path.join(mag_path, image)
                dest_path = os.path.join(root_dst_dir, mag)
                if not os.path.exists(dest_path):
                    os.makedirs(dest_path)
                assert(not os.path.isfile(os.path.join(dest_path, image)))
                shutil.move(image_path, dest_path)

            assert (not os.listdir(mag_path))
            os.rmdir(mag_path)
            # exit(1)
