import os
import shutil
import numpy as np
import random

mag = '200X'
# root_src_dir = os.path.join(mag, 'SOB')
root_dst_dir = os.path.join('split_data', mag)
# if not os.path.exists(root_dst_dir):
#     os.makedirs(root_dst_dir)

test_percent = 0.2

for type_name in ['benign', 'malignant']:
    root_src_dir = os.path.join(mag, type_name)
    # dest_dir = os.path.join(root_dst_dir, type_name)


    files = os.listdir(root_src_dir)
    random.shuffle(files)

    num_files = len(files)

    test_files = files[:int(test_percent*num_files)]
    train_files = files[int(test_percent*num_files):]

    train_dest_dir = os.path.join(root_dst_dir, 'train', type_name)
    test_dest_dir = os.path.join(root_dst_dir, 'test', type_name)
    if not os.path.exists(train_dest_dir):
        os.makedirs(train_dest_dir)

    if not os.path.exists(test_dest_dir):
        os.makedirs(test_dest_dir)

    for file_name in train_files:
        shutil.copy(os.path.join(root_src_dir, file_name), train_dest_dir)

    for file_name in test_files:
        shutil.copy(os.path.join(root_src_dir, file_name), test_dest_dir)
    # print(len(files))

    # if not os.path.exists(root_dst_dir):
    #     os.makedirs(root_dst_dir)


#
#
# for folder in os.listdir(root_src_dir):
#     print(folder)
#     subdir = os.path.join(root_src_dir, folder)
#     assert(not os.path.isfile(subdir))
#     for dir in os.listdir(subdir):
#         path = os.path.join(subdir, dir)
#         print(path)
#         for mag in os.listdir(path):
#             mag_path = os.path.join(path, mag)
#             print(mag_path)
#             for image in os.listdir(mag_path):
#                 image_path = os.path.join(mag_path, image)
#                 dest_path = os.path.join(root_dst_dir, mag)
#                 if not os.path.exists(dest_path):
#                     os.makedirs(dest_path)
#                 assert(not os.path.isfile(os.path.join(dest_path, image)))
#                 shutil.move(image_path, dest_path)
#
#             assert (not os.listdir(mag_path))
#             os.rmdir(mag_path)
#             # exit(1)
