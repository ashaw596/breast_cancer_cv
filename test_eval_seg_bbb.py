import os
import random

from PIL import Image
from keras.optimizers import RMSprop
from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model
import numpy as np
import matplotlib.pyplot as plt
from os.path import join

import keras as K
import tensorflow as tf

from models import AtrousFCN_Resnet50_16s

with tf.Session() as sess:
    with sess.as_default():
        K.backend.tensorflow_backend.set_session(sess)

        # datagen = ImageDataGenerator(
        #         # zca_whitening=True,
        #         samplewise_center=True,
        #         samplewise_std_normalization=True,
        #         rotation_range=0,
        #         width_shift_range=0,
        #         height_shift_range=0,
        #         rescale=1./255,
        #         shear_range=0,
        #         zoom_range=0,
        #         horizontal_flip=False,
        #         vertical_flip=False,
        #         fill_mode='nearest')
        #
        # magnification = '40X'
        def make_folder_if_not_exists(path):
            if not os.path.isdir(path):
                os.mkdir(path)
        root = 'BBBC005_png2017-10-18_01-04-15'
        images_path = join('BBBC005_png', 'BBBC005_v1_images')
        ground_truth_path = join('BBBC005_png', 'BBBC005_v1_ground_truth')
        save_image_dir = join(root, 'images')
        make_folder_if_not_exists(save_image_dir)

        train_filenames = np.load(join(root, 'train_filenames.npy'))
        test_filenames = np.load(join(root, 'test_filenames.npy'))
        print(len(test_filenames))
        # os.mkdir(save_root)

        # ground_truth_path = join(root_dir, 'BBBC005_v1_ground_truth')
        # images_path = join(root_dir, 'BBBC005_v1_images')

        # data_dir = os.path.join('reordered_data', 'patient_split', magnification)

        # filelist = []
        # for folder in ["benign", 'malignant']:
        #     f_path = os.path.join(data_dir, 'train', folder)
        #     for file in os.listdir(f_path):
        #         file_path = os.path.join(f_path, file)
        #         filelist.append(file_path)
        #
        # x = np.array([np.array(Image.open(fname)) for i, fname in enumerate(filelist) if i%1==0])
        # datagen.fit(x)

        # batch_size = 471

        # model = load_model('my_model.h5')
        # model = load_model('patient_split.h5')

        # height, width  = (460, 700)

        input_shape=(520, 696)
        # target_shape = (300, 300)
        target_shape = input_shape
        model = AtrousFCN_Resnet50_16s(input_shape=(target_shape[0],target_shape[1], 3), classes=2)
        # model = load_model(join(root, 'model.h5'))

        # for layer1, layer2 in zip(model.layers, segment_model.layers):
        #     layer1.set
        # model.set_weights(segment_model.get_weights())
        # 1/0
        # for layer1, layer2 in zip(model.layers, segment_model.layers):
        #     layer1.set_weights(layer2.get_weights())
        # 1/0
        # test_generator = datagen.flow_from_directory(
        #         os.path.join(data_dir, 'test'),
        #         target_size=(height, width),
        #         batch_size=batch_size,
        #         class_mode='categorical',  # this means our generator will only yield batches of data, no labels
        #         shuffle=False)

        def read_files(filename):
            image_filename = images_path +'\\' + filename
            ground_truth_filename = ground_truth_path +'\\' + filename
            image_string = tf.read_file(image_filename)
            ground_truth_string = tf.read_file(ground_truth_filename)
            image_decoded = tf.image.decode_image(image_string)
            ground_truth_decoded = tf.image.decode_image(ground_truth_string)

            return image_decoded, ground_truth_decoded

        def crop(image_decoded, ground_truth_decoded):
            print(ground_truth_decoded)
            print(image_decoded)

            # image_decoded = tf.cast(image_decoded, tf.int32)
            together = tf.concat([image_decoded, ground_truth_decoded], axis=2)
            cropped = tf.random_crop(together, size=list(target_shape) + [2])
            image = tf.image.grayscale_to_rgb(tf.slice(cropped, [0, 0, 0], [-1, -1, 1]))
            image = tf.image.convert_image_dtype(image, tf.float32)
            # image = tf.cast(image, tf.float32)/255.0

            truth = tf.cast(tf.cast(tf.slice(cropped, [0, 0, 1], [-1, -1, 1]), tf.bool), tf.int32)
            return image, truth

        def softmax_sparse_crossentropy(y_true, y_pred):
            # y_pred = tf.reshape(y_pred, (-1, target_shape[0]*target_shape[1]))
            # log_softmax_pred = tf.nn.log_softmax(y_pred)

            y_true = tf.reshape(y_true,(-1, target_shape[0], target_shape[1]))
            # log_softmax_true = tf.nn.softmax(y_true)
            # unpacked = tf.unstack(y_true, axis=-1)
            # y_true = tf.stack(unpacked[:-1], axis=-1)
            # print(y_true)

            return tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = tf.cast(y_true, tf.int32), logits = y_pred))
        def softmax(array):
            array = np.clip(array, -np.inf, 100)
            # print(array)
            e = np.exp(array)
            e+= 1e-32
            dist = e / np.expand_dims(np.sum(e, axis=-1), axis=-1)
            return np.array(dist, dtype=np.float)

            # return image_decoded, ground_truth_decoded
        filename_placeholder = tf.placeholder(tf.string)
        image_decoded, ground_truth_decoded = read_files(filename_placeholder)
        cropped_image, cropped_truth = crop(image_decoded, ground_truth_decoded)

        optimizer = RMSprop(lr=0.001)
        model.compile(loss=softmax_sparse_crossentropy,
                          optimizer=optimizer,
                          metrics=['sparse_categorical_accuracy'])
        # init_op = tf.global_variables_initializer()

        # Initialize the variables (like the epoch counter).
        # sess.run(init_op)

        model.load_weights(join(root, 'model.h5'))
        total_percent = 0
        for filename in test_filenames:
            img, truth = sess.run([cropped_image, cropped_truth], feed_dict={filename_placeholder:filename})
            # truth = np.squeeze(truth)
            print(img.shape)
            print(truth.shape)
            # print(model.test_on_batch(np.expand_dims(img, axis=0), np.expand_dims(truth, axis=0)))
            # print(img)
            pred = model.predict(np.expand_dims(img, axis=0))
            # print(pred)
            pred = softmax(np.squeeze(pred))
            # print (pred)

            # print(np.max(img))
            # print(np.min(img))
            # print(pred.shape)

            pred_index = np.argmax(pred, axis=2)
            num_incorrect = np.sum(np.logical_xor(pred_index, np.squeeze(truth)))
            num_pixels = input_shape[0] * input_shape[1]
            num_correct = num_pixels - num_incorrect

            percent = num_correct/num_pixels
            print('percent', percent)
            total_percent += percent
            # save_image_dir = None

            if save_image_dir:

                # plt.figure("not")
                # plt.imshow(img * pred[:, :, 0:1])
                # plt.savefig(os.path.join(save_image_dir, filename + 'not' + "{:.4f}".format(percent) + '.png'))
                #
                # plt.figure("cell")
                # plt.imshow(img * pred[:, :, 1:2])
                # plt.savefig(os.path.join(save_image_dir, filename + 'cell' + "{:.4f}".format(1-percent) + '.png'))
                plt.figure("cell")
                plt.imshow(pred[:, :, 1], cmap='gray')
                plt.savefig(os.path.join(save_image_dir, filename + 'cell' + "{:.4f}".format(percent) + '.png'))

                plt.figure("org")
                plt.imshow(img)
                plt.savefig(os.path.join(save_image_dir, filename + 'org' + '.png'))

        print('total_percent', total_percent/len(test_filenames))








    # X, Y = test_generator.next()

    # split = 1
    # patch_width = 700//split
    # patch_height = 460//split
    # # print(X.shape)
    #
    # # assert(height/split == patch_height)
    # #
    # # assert(width/split == patch_width)
    #
    # num_correct = 0
    # total = batch_size
    # # save_image_dir = None
    # save_image_dir = os.path.join("images", "AtrousFCN_Resnet50_16s_segmentation_patient_split_4")
    #
    # def make_folder_if_not_exists(path):
    #     if not os.path.isdir(path):
    #         os.mkdir(path)
    #
    # if save_image_dir:
    #     make_folder_if_not_exists(save_image_dir)
    #
    # outputs = model.predict(X)
    # for i, input, label, output in zip(range(len(X)), X, Y, outputs):
    #
    #     print(type(input))
    #     # patches = []
    #     # for i in range(split):
    #     #     x = i*patch_width
    #     #     for j in range(split):
    #     #         y = j*patch_height
    #     #         im = input[y:y+patch_height,x:x+patch_width, :]
    #     #         patches.append(im)
    #     # output = model.predict(np.expand_dims(np.array(input), axis=0))
    #     output = softmax(output)
    #
    #     s = np.sum(np.reshape(output, [-1, 2]), axis=0)
    #     # print(s)
    #     #
    #     s = s / np.sum(s)
    #     # print(s)
    #
    #     print(np.argmax(s))
    #     # print(np.argmax(label))
    #     correct = np.argmax(s) == np.argmax(label)
    #
    #     # if not correct:
    #
    #     #     plt.show()
    #
    #     if save_image_dir:
    #         label_name = 'benign' if np.argmax(label) == 0 else 'malignant'
    #         correct_name = 'correct' if correct else 'incorrect'
    #
    #         label_dir = os.path.join(save_image_dir, label_name)
    #         make_folder_if_not_exists(label_dir)
    #
    #         correct_path = os.path.join(label_dir, correct_name)
    #         make_folder_if_not_exists(correct_path)
    #         # print(output[:, :, :])
    #         input = input/4.0 + 0.5
    #         plt.figure("benign")
    #         plt.imshow(input * output[:, :, 0:1])
    #         plt.savefig(os.path.join(correct_path, str(i) + 'benign' + "{:.4f}".format(s[0]) + '.png'))
    #         plt.figure("malignant")
    #         plt.imshow(input * output[:, :, 1:2])
    #         plt.savefig(os.path.join(correct_path, str(i) + 'malignant' + "{:.4f}".format(s[1]) + '.png'))
    #
    #         plt.figure("org")
    #         plt.imshow(input)
    #         plt.savefig(os.path.join(correct_path, str(i) + 'org' + '.png'))
    #
    #         # plt.show()
    #
    #     print('label', label)
    #     print('s', s)
    #     print('correct', correct)
    #     num_correct += correct
    #
    # print('num_correct', num_correct)
    # print('num_correct/total', num_correct/total)

    # num_correct = 0
    # total = batch_size
    #
    # num_patches = 1000
    #
    # for input, label in zip(X, Y):
    #
    #
    #     patches = []
    #     for i in range(num_patches):
    #         x = random.randrange(0, width-patch_width)
    #         y = random.randrange(0, height-patch_height)
    #         im = input[y:y+patch_height,x:x+patch_width, :]
    #         patches.append(im)
    #
    #     output = model.predict(np.array(patches))
    #     s = np.sum(output, axis=0)
    #
    #     # print(np.argmax(s))
    #     # print(np.argmax(label))
    #     correct = np.argmax(s) == np.argmax(label)
    #
    #     print('label', label)
    #     print('s', s)
    #     print('correct', correct)
    #     num_correct += correct
    #
    # print(num_patches, " random patches")
    # print('num_correct', num_correct)
    # print('num_correct/total', num_correct/total)