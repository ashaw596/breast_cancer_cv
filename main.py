from keras.callbacks import ModelCheckpoint
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Flatten, GlobalAveragePooling2D
from keras.layers import Conv2D, MaxPooling2D
from scipy import ndimage
import os
os.getcwd()
import glob
import scipy
from PIL import Image
import numpy as np

from keras.preprocessing.image import ImageDataGenerator
from keras.applications.vgg16 import VGG16

datagen = ImageDataGenerator(
        zca_whitening=True,
        rotation_range=0,
        width_shift_range=0.4,
        height_shift_range=0.4,
        rescale=1./255,
        shear_range=0,
        zoom_range=[0.2,0.2],
        horizontal_flip=True,
        vertical_flip=True,
        fill_mode='nearest')

magnification = '40X'

# data_dir = os.path.join('reordered_data', 'split_data', magnification)
data_dir = os.path.join('reordered_data', 'patient_split', magnification)

save_name = "VGG_patient_split_" + magnification + "_4.h5"
batch_size = 100

train_generator = datagen.flow_from_directory(
        os.path.join(data_dir, 'train'),
        target_size=(92, 140),
        batch_size=batch_size,
        class_mode='categorical',  # this means our generator will only yield batches of data, no labels
        shuffle=True)

test_generator = datagen.flow_from_directory(
        os.path.join(data_dir, 'test'),
        target_size=(92, 140),
        batch_size=batch_size,
        class_mode='categorical',  # this means our generator will only yield batches of data, no labels
        shuffle=True)

# base_model = VGG16(weights='imagenet', include_top=False)
base_model = VGG16(weights='imagenet', include_top=False, input_shape=(92, 140, 3))

# add a global spatial average pooling layer
x = base_model.output
x = GlobalAveragePooling2D()(x)
# let's add a fully-connected layer
x = Dense(256, activation='relu')(x)
# and a logistic layer -- let's say we have 200 classes
predictions = Dense(2, activation='softmax')(x)


# this is the model we will train
model = Model(inputs=base_model.input, outputs=predictions)

# model = Sequential()
#
# model.add(Conv2D(32, (3, 3), activation='relu', input_shape=(92, 140, 3)))
# model.add(Conv2D(32, (3, 3), activation='relu'))
# model.add(MaxPooling2D(pool_size=(2, 2)))
# model.add(Dropout(0.25))
#
# model.add(Flatten())
# model.add(Dense(128, activation='relu'))
# model.add(Dropout(0.5))
# model.add(Dense(2, activation='softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

modelCheckpoint = ModelCheckpoint(filepath=save_name)
model.fit_generator(
        train_generator,
        steps_per_epoch=10000 // batch_size,
        epochs=50,
        validation_data=test_generator,
        validation_steps=5,
        callbacks=[modelCheckpoint])


model.save(save_name)
# model.fit(X_train, Y_train,
#           batch_size=32, nb_epoch=10, verbose=1)
# for images, label in generator:
#         for im in images:
#                 img = Image.fromarray((im*255).astype(np.uint8), 'RGB')
#                 img.show()
#                 print(im.shape)
#
#         exit(1)
#         print(label)
# print(generator)