from datetime import datetime
from keras.callbacks import ModelCheckpoint
from keras.losses import categorical_crossentropy
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Flatten, GlobalAveragePooling2D
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import RMSprop
from scipy import ndimage
import os

from models import AtrousFCN_Resnet50_16s
from utils.loss_function import binary_crossentropy_with_logits
from tqdm import tqdm


os.getcwd()
import glob
import scipy
from PIL import Image
import numpy as np
from keras.models import load_model

from keras.preprocessing.image import ImageDataGenerator
from keras.applications.vgg16 import VGG16
from keras.applications.inception_v3 import InceptionV3
import keras as K
import tensorflow as tf
import random
from os.path import join

with tf.Session() as sess:
    with sess.as_default():
        K.backend.tensorflow_backend.set_session(sess)

        startTime = datetime.now()
        zoom_constant = 1
        input_shape=(520, 696)
        target_shape=(300,300)

        batch_size = 16
        def read_files(filename):
            image_filename = images_path +'\\' + filename
            ground_truth_filename = ground_truth_path +'\\' + filename
            image_string = tf.read_file(image_filename)
            ground_truth_string = tf.read_file(ground_truth_filename)
            image_decoded = tf.image.decode_image(image_string)
            ground_truth_decoded = tf.image.decode_image(ground_truth_string)

            return image_decoded, ground_truth_decoded

        def crop(image_decoded, ground_truth_decoded):
            print(ground_truth_decoded)
            print(image_decoded)

            # image_decoded = tf.cast(image_decoded, tf.int32)
            together = tf.concat([image_decoded, ground_truth_decoded], axis=2)
            cropped = tf.random_crop(together, size=(300, 300, 2))
            image = tf.image.grayscale_to_rgb(tf.slice(cropped, [0, 0, 0], [-1, -1, 1]))
            image = tf.image.convert_image_dtype(image, tf.float32)
            # image = tf.cast(image, tf.float32)/255.0

            truth = tf.cast(tf.cast(tf.slice(cropped, [0, 0, 1], [-1, -1, 1]), tf.bool), tf.int32)
            return image, truth
        root_dir = 'BBBC005_png'
        save_root = root_dir + startTime.strftime("%Y-%m-%d_%H-%M-%S")

        os.mkdir(save_root)

        ground_truth_path = join(root_dir, 'BBBC005_v1_ground_truth')
        images_path = join(root_dir, 'BBBC005_v1_images')

        test_percent = 0.3

        filenames = np.array(os.listdir(ground_truth_path))
        np.random.shuffle(filenames)

        num_testing = int(len(filenames) *test_percent)
        test_filenames = filenames[:num_testing]
        train_filenames = filenames[num_testing:]

        np.save(join(save_root,'train_filenames.npy'), train_filenames)
        np.save(join(save_root,'test_filenames.npy'), test_filenames)
        print(train_filenames)
        filename_queue = tf.train.string_input_producer(train_filenames.tolist())
        print(filename_queue)
        filename = filename_queue.dequeue()
        image_decoded, ground_truth_decoded = read_files(filename)
        cropped_image, cropped_truth = crop(image_decoded, ground_truth_decoded)

        batch_image, batch_truth  = tf.train.batch(tensors=[cropped_image, cropped_truth], batch_size=batch_size, capacity=10)


        def softmax_sparse_crossentropy(y_true, y_pred):
            # y_pred = tf.reshape(y_pred, (-1, target_shape[0]*target_shape[1]))
            # log_softmax_pred = tf.nn.log_softmax(y_pred)

            y_true = tf.reshape(y_true,(-1, target_shape[0], target_shape[1]))
            # log_softmax_true = tf.nn.softmax(y_true)
            # unpacked = tf.unstack(y_true, axis=-1)
            # y_true = tf.stack(unpacked[:-1], axis=-1)
            # print(y_true)

            return tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels = tf.cast(y_true, tf.int32), logits = y_pred))
            #
            # cross_entropy = -K.sum(y_true * log_softmax, axis=1)
            # cross_entropy_mean = K.mean(cross_entropy)

            # return cross_entropy_mean


        # datagen = ImageDataGenerator(
        #         # zca_whitening=True,
        #         samplewise_center=True,
        #         samplewise_std_normalization=True,
        #         rotation_range=0,
        #         width_shift_range=0,
        #         height_shift_range=0,
        #         rescale=1./255,
        #         shear_range=0,
        #         zoom_range=[1,1],
        #         horizontal_flip=True,
        #         vertical_flip=True,
        #         fill_mode='nearest')
        #
        # magnification = '40X'

        # data_dir = os.path.join('reordered_data', 'split_data', magnification)
        # data_dir = os.path.join('reordered_data', 'patient_split', magnification)
        save_name = join(save_root, 'model.h5')
        # save_name = "AtrousFCN_Resnet50_16s_segmentation_patient_split_" + magnification + "_4.h5"
        #
        # filelist = []
        # for folder in ["benign", 'malignant']:
        #     f_path = os.path.join(data_dir, 'train', folder)
        #     for file in os.listdir(f_path):
        #         file_path = os.path.join(f_path, file)
        #         filelist.append(file_path)
        #
        # x = np.array([np.array(Image.open(fname)) for i, fname in enumerate(filelist) if i%1==0])
        # datagen.fit(x)


        # train_generator = datagen.flow_from_directory(
        #         os.path.join(data_dir, 'train'),
        #         target_size=input_shape,
        #         batch_size=batch_size,
        #         class_mode='sparse',  # this means our generator will only yield batches of data, no labels
        #         shuffle=True)
        #
        # test_generator = datagen.flow_from_directory(
        #         os.path.join(data_dir, 'test'),
        #         target_size=input_shape,
        #         batch_size=batch_size,
        #         class_mode='sparse',  # this means our generator will only yield batches of data, no labels
        #         shuffle=True)

        # train_generator = SegmentationGenerator(train_generator, target_shape=target_shape)
        # test_generator = SegmentationGenerator(test_generator, target_shape=target_shape)
        # model = load_model("VGG_imagenet_dropout_patient_split_" + magnification + "_1_more.h5")
        # this is the model we will train

        # model = Sequential()
        #
        # model.add(Conv2D(32, (3, 3), activation='relu', input_shape=(92, 140, 3)))
        # model.add(Conv2D(32, (3, 3), activation='relu'))
        # model.add(MaxPooling2D(pool_size=(2, 2)))
        # model.add(Dropout(0.25))
        #
        # model.add(Flatten())
        # model.add(Dense(128, activation='relu'))
        # model.add(Dropout(0.5))
        # model.add(Dense(2, activation='softmax'))
        # root = 'BBBC005_png2017-10-18_02-39-16'
        model = AtrousFCN_Resnet50_16s(input_shape=(target_shape[0], target_shape[1], 3), classes=2)


        # next(train_generator)
        # 1/0
        # print(model)
        # print(model.output)

        #
        # base_model = VGG16(weights='imagenet', include_top=False, input_shape=(460//5, 700//5, 3))
        # # base_model = InceptionV3(weights=None, include_top=False, input_shape=(460//2, 700//2, 3))
        #
        # # add a global spatial average pooling layer
        # x = base_model.output
        # x = GlobalAveragePooling2D()(x)
        # # let's add a fully-connected layer
        # x = Dense(256, activation='relu')(x)
        # x = Dropout(0.5, noise_shape=None, seed=None)(x)
        #
        # # and a logistic layer -- let's say we have 200 classes
        # predictions = Dense(2, activation='softmax')(x)
        # model = Model(inputs=base_model.input, outputs=predictions)

        filename_placeholder = tf.placeholder(tf.string)
        im, g = read_files(filename_placeholder)
        cropped_im, cropped_g = crop(im, g)

        # init_op = tf.global_variables_initializer()

        # Initialize the variables (like the epoch counter).
        optimizer = RMSprop(lr=0.001)
        model.compile(loss=softmax_sparse_crossentropy,
                          optimizer=optimizer,
                          metrics=['sparse_categorical_accuracy'])
        # sess.run(init_op)

        # modelCheckpoint = ModelCheckpoint(filepath=save_name)
        epochs = 1000

        # Start input enqueue threads.
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess=sess, coord=coord)


        # model.load_weights(join(root, 'model.h5'))

        try:
            for e in tqdm(range(epochs)):
                losses = np.zeros([2])
                num_iterations = 20
                for i in range(num_iterations):
                    # print(sess.run([cropped_image]))
                    x, y = sess.run([batch_image, batch_truth])
                    # print(e)
                    loss = model.train_on_batch(x, y)
                    losses += loss
                    # pred = model.predict(x)
                    # # print (pred)
                    # # print(loss)
                    # x, y = sess.run([cropped_im, cropped_g], feed_dict={filename_placeholder: np.random.choice(test_filenames)})
                    # print('tsets', model.test_on_batch(np.expand_dims(x, axis=0), np.expand_dims(y, axis=0)))
                losses /= num_iterations
                print('loss', losses)
                print(e)
                if e % 20 == 0:
                    model.save_weights(save_name)


        except tf.errors.OutOfRangeError as e:
            print(e)
        finally:
            # When done, ask the threads to stop.
            coord.request_stop()

        #
        # model.fit_generator(
        #         train_generator,
        #         steps_per_epoch=1000 // batch_size,
        #         epochs=1000,
        #         validation_data=test_generator,
        #         validation_steps=10,
        #         # use_multiprocessing = True,
        #         callbacks=[modelCheckpoint])














        # model.fit(X_train, Y_train,
        #           batch_size=32, nb_epoch=10, verbose=1)
        # for images, label in generator:
        #         for im in images:
        #                 img = Image.fromarray((im*255).astype(np.uint8), 'RGB')
        #                 img.show()
        #                 print(im.shape)
        #
        #         exit(1)
        #         print(label)
        # print(generator)