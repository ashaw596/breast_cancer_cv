import os
import random

import tensorflow as tf
from tensorflow.python.framework.errors_impl import OutOfRangeError

from tensorflow.python.keras.callbacks import ReduceLROnPlateau
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
from tensorflow.python.keras.models import Sequential, Model
from tensorflow.python.keras.layers import Dense, Dropout, Activation, Flatten, GlobalAveragePooling2D
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.applications.vgg16 import VGG16


import numpy as np

import matplotlib.pyplot as plt

def write_jpeg(data, filepath):
    g = tf.Graph()
    with g.as_default():
        data_t = tf.placeholder(tf.uint8)
        op = tf.image.encode_jpeg(data_t, format='rgb', quality=100)
        init = tf.initialize_all_variables()

    with tf.Session(graph=g) as sess:
        sess.run(init)
        data_np = sess.run(op, feed_dict={ data_t: data })

    with open(filepath, 'w') as fd:
        fd.write(data_np)
image_size = [904, 1224, 1]
splits = 8

def parse_fn(example):
  "Parse TFExample records and perform simple data augmentation."
  example_fmt = {
    "image_raw": tf.FixedLenFeature((), tf.string, ""),
    "mag_objective": tf.FixedLenFeature((), tf.int64, 0),
    "file_name": tf.FixedLenFeature((), tf.string, ""),
    "well_id": tf.FixedLenFeature((), tf.string, ""),
    "well_location": tf.FixedLenFeature((), tf.int64, 0),
    "image_type": tf.FixedLenFeature((), tf.string, ""),
    "day": tf.FixedLenFeature((), tf.int64, 0),
    "train": tf.FixedLenFeature((), tf.int64, 0),
    "dir_name": tf.FixedLenFeature((), tf.string, ""),
    "experiment_start_date_str": tf.FixedLenFeature((), tf.string, ""),
    "experiment_start_time_str": tf.FixedLenFeature((), tf.string, ""),
    "experiment_name": tf.FixedLenFeature((), tf.string, ""),
    "tissue_type": tf.FixedLenFeature((), tf.string, "")
  }
  parsed = tf.parse_single_example(example, example_fmt)
  image = tf.decode_raw(parsed["image_raw"], tf.uint8)
  image = tf.reshape(image, image_size)
  label = tf.equal(parsed["tissue_type"], b"cord_tissue")
  image = tf.image.convert_image_dtype(image, dtype=tf.uint8)

  return image, label, parsed


def extract_image_patches_fn(splits, height, width, channels):
  def temp(images, label):
    images = tf.expand_dims(images, axis=0)
    sizes = [1, height//splits, width//splits, channels]
    patches = tf.extract_image_patches(images, ksizes=sizes, strides=[1, height//splits, width//splits, 1], rates=[1,1,1,1], padding="VALID")
    labels = tf.tile(tf.expand_dims(label, 0), [splits*splits])
    patches = tf.reshape(patches, [splits*splits, sizes[1], sizes[2],sizes[3]])
    # patches = image_to_patches(images, height, width, height//splits, width//splits)
    print(patches, labels)
    labels = tf.cast(labels, tf.int32)
    labels = tf.one_hot(labels, 2)
    patches = tf.image.grayscale_to_rgb(patches)
    return (patches, labels)
  return temp

folder_name = '''tissue_type_split_tfrecord_Phase Contrast_10x_1'''
out_folder = os.path.join(os.getcwd(), "viz", folder_name)

os.makedirs(out_folder)

data_dir = os.path.join(os.getcwd(), 'data', '180328_121316_6WP MSC Imaging', 'new', folder_name)

test_tfrecords = os.path.join(data_dir, "test-*.tfrecords")

def images_fn():
  test_tfrecords_list = tf.data.Dataset.list_files(test_tfrecords)
  test_dataset = test_tfrecords_list.interleave(tf.data.TFRecordDataset, cycle_length=1, block_length=1)
  test_dataset = test_dataset.map(map_func=parse_fn)
  iterator  = test_dataset.make_one_shot_iterator()
  return iterator.get_next()

def test_input_fn():
  test_tfrecords_list = tf.data.Dataset.list_files(test_tfrecords)
  test_dataset = test_tfrecords_list.interleave(tf.data.TFRecordDataset, cycle_length=1, block_length=1)
  test_dataset = test_dataset.map(map_func=parse_fn)
  test_dataset = test_dataset.map(extract_image_patches_fn(splits, image_size[0], image_size[1], image_size[2]))
  test_dataset = test_dataset.map(map_func= lambda x,y: ({'input_1': x}, y))
  iterator  = test_dataset.make_one_shot_iterator()
  return iterator.get_next()

image, label, parsed_tf = images_fn()

image_jpeg_tf = tf.image.encode_jpeg(tf.image.grayscale_to_rgb(image), format='rgb', quality=100)
with tf.Session() as sess:
    for i in range(10000):
        try:
          image_jpeg, lab, parsed = sess.run([image_jpeg_tf, label, parsed_tf])
          for key in parsed:
              if key == 'image_raw':
                  continue
              if isinstance(parsed[key], bytes):
                  parsed[key] = parsed[key].decode()
          file_name = "{}__tissueType-{}_day-{}_mag-{}_wellId-{}_wellLoc-{}_imageType-{}.jpg"\
              .format(parsed['experiment_name'], parsed['tissue_type'], parsed['day'], parsed['mag_objective'],
                      parsed['well_id'], parsed['well_location'], parsed['image_type'])
          with open(os.path.join(out_folder, file_name), 'wb') as fd:
              fd.write(image_jpeg)
          print(i)
        except OutOfRangeError:
          print("OutOfRangeError")
          break