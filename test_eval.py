import os
import random

from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model
import numpy as np

datagen = ImageDataGenerator(
        zca_whitening=True,
        rotation_range=0,
        width_shift_range=0,
        height_shift_range=0,
        rescale=1./255,
        shear_range=0,
        zoom_range=0,
        horizontal_flip=False,
        vertical_flip=False,
        fill_mode='nearest')

magnification = '40X'

data_dir = os.path.join('reordered_data', 'patient_split', magnification)

batch_size = 471

# model = load_model('my_model.h5')

# model = load_model('patient_split.h5')

model = load_model('VGG_patient_split_40X_1.h5')

height, width  = (460, 700)
test_generator = datagen.flow_from_directory(
        os.path.join(data_dir, 'test'),
        target_size=(height, width),
        batch_size=batch_size,
        class_mode='categorical',  # this means our generator will only yield batches of data, no labels
        shuffle=False)

X, Y = test_generator.next()

patch_width = 140
patch_height = 92
# print(X.shape)
split = 5

assert(height/split == patch_height)

assert(width/split == patch_width)

# num_correct = 0
# total = batch_size
#
# for input, label in zip(X, Y):
#
#     patches = []
#     for i in range(split):
#         x = i*patch_width
#         for j in range(split):
#             y = j*patch_height
#             im = input[y:y+patch_height,x:x+patch_width, :]
#             patches.append(im)
#     output = model.predict(np.array(patches))
#     s = np.sum(output, axis=0)
#
#     # print(np.argmax(s))
#     # print(np.argmax(label))
#     correct = np.argmax(s) == np.argmax(label)
#
#     print('label', label)
#     print('s', s)
#     print('correct', correct)
#     num_correct += correct
#
# print('num_correct', num_correct)
# print('num_correct/total', num_correct/total)

num_correct = 0
total = batch_size

num_patches = 1000

for input, label in zip(X, Y):


    patches = []
    for i in range(num_patches):
        x = random.randrange(0, width-patch_width)
        y = random.randrange(0, height-patch_height)
        im = input[y:y+patch_height,x:x+patch_width, :]
        patches.append(im)

    output = model.predict(np.array(patches))
    s = np.sum(output, axis=0)

    # print(np.argmax(s))
    # print(np.argmax(label))
    correct = np.argmax(s) == np.argmax(label)

    print('label', label)
    print('s', s)
    print('correct', correct)
    num_correct += correct

print(num_patches, " random patches")
print('num_correct', num_correct)
print('num_correct/total', num_correct/total)