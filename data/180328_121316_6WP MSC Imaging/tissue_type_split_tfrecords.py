import os
import shutil
import numpy as np
import random
import tensorflow as tf
import parse

import os
from PIL import Image

def dict_to_example(dictionary: dict):
    new_dict = {}
    for key, value in dictionary.items():
        if isinstance(value, str):
            value = _bytes_feature(value.encode())
        elif isinstance(value, int):
            value = _int64_feature(value)
        elif not isinstance(value, tf.train.Feature):
            print(key, value)
            raise "Unrecognized Feature type"
        new_dict[key] = value
    return tf.train.Example(features=tf.train.Features(feature=new_dict))

def _int64_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def image_to_feature(file_name):
    try:
        im = Image.open(file_name)
        im.mode = 'I'
        im.thumbnail(im.size)
        return _bytes_feature(im.point(lambda i: i * (1. / 256)).convert('L').tobytes())
    except Exception as e:
        print('Exception', e)

# mag = '200X'

script_dir  = os.path.dirname(os.path.realpath(__file__))
# print(script_dir)
root_src_dir = os.path.join(script_dir, 'org_data')

#root_dst_dir = os.path.join(script_dir, 'new', 'tissue_type_folder_split_phasecontrast_4x_4-5-6')

#if os.path.exists(root_dst_dir):
#    raise Exception("Destination path already exits")


test_percent = 0.3
dirs = os.listdir(root_src_dir)
cords = []
bones = []
for dir in dirs:
    if 'GMP' in dir:
        cords.append(dir)
    elif 'RB' in dir:
        bones.append(dir)


random.shuffle(cords)
random.shuffle(bones)

train_cords_size = int(len(cords)*(1.0-test_percent))
train_cords = cords[:train_cords_size]
test_cords = cords[train_cords_size:]

train_bone_size = int(len(bones)*(1.0 - test_percent))
train_bones = bones[:train_bone_size]
test_bones = bones[train_bone_size:]

train = train_cords + train_bones
test = test_cords + test_bones

train = list(zip(train, ['train']*len(train)))
test = list(zip(test, ['test']*len(test)))

all = train + test
for dir, train_test in all:
    cur_path = os.path.join(root_src_dir, dir)
    dir_parsed = parse.parse("{start_date}_{start_time}_{experiment_name}", dir)

    #TODO: fix this. This won't always be valid
    if 'GMP' in dir_parsed['experiment_name']:
        dir_type = 'cord_tissue'
    elif 'RB' in dir:
        dir_type = 'bone_marrow'
    else:
        raise Exception("You did something wrong")
    print(dir_parsed)

    file_list = []
    for file in (name for name in os.listdir(cur_path) if name.endswith('.tif')):
        cur_path = os.path.join(root_src_dir, dir, file)
        print(file)
        parsed = parse.parse("{well_id}_{objective:d}_{unused}_{well_location:d}_{image_type}_{day:d}.tif", file)

        features = dict(parsed.named)
        features.pop('unused')
        features['file_name'] = file

        print(features)
        if features['objective'] == 2:
            features['objective'] = 4
        elif features['objective'] == 3:
            features['objective'] = 10
        else:
            raise "unrecognized objective"

        features['dir_name'] = dir
        features['experiment_start_date_str'] = dir_parsed['start_date']
        features['experiment_start_time_str'] = dir_parsed['start_time']
        features['experiment_name'] = dir_parsed['experiment_name']

        img_raw = image_to_feature(os.path.join(cur_path))
        features['image_raw'] = img_raw
        features['tissue_type'] = dir_type
        example = dict_to_example(features)
        print(example)



train_cords_dest = os.path.join(root_dst_dir, 'train', 'cord_tissue')
test_cords_dest = os.path.join(root_dst_dir, 'test', 'cord_tissue')
train_bones_dest = os.path.join(root_dst_dir, 'train', 'bones_dest')
test_bones_dest = os.path.join(root_dst_dir, 'test', 'bones_dest')

print(len(cords), len(bones))
# exit()
random.shuffle(cords)
random.shuffle(bones)

os.makedirs(train_cords_dest)
os.makedirs(test_cords_dest)
os.makedirs(train_bones_dest)
os.makedirs(test_bones_dest)

train_cords_size = int(len(cords)*(1.0-test_percent))
train_cords = cords[:train_cords_size]
test_cords = cords[train_cords_size:]

train_bone_size = int(len(bones)*(1.0 - test_percent))
train_bones = bones[:train_bone_size]
test_bones = bones[train_bone_size:]

def flatten(list_of_lists):
    return (item for sublist in list_of_lists for item in sublist)



for path, name in flatten(train_cords):
    #label =
    img_feature = image_to_feature(path)

for path, name in flatten(test_cords):
    convert(path, os.path.join(test_cords_dest, name.split(".tif")[0] + ".png"))


for path, name in flatten(train_bones):
    convert(path, os.path.join(train_bones_dest, name.split(".tif")[0] + ".png"))
for path, name in flatten(test_bones):
    convert(path, os.path.join(test_bones_dest, name.split(".tif")[0] + ".png"))

