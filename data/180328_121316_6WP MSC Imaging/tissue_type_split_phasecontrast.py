import os
import shutil
from collections import defaultdict

import numpy as np
import random
import tensorflow as tf
import parse
import itertools

import os
from PIL import Image

def dict_to_example(dictionary: dict):
    new_dict = {}
    for key, value in dictionary.items():
        if isinstance(value, str):
            value = _bytes_feature(value.encode())
        elif isinstance(value, int):
            value = _int64_feature(value)
        elif not isinstance(value, tf.train.Feature):
            print(key, value)
            raise "Unrecognized Feature type"
        new_dict[key] = value
    return tf.train.Example(features=tf.train.Features(feature=new_dict))

def _int64_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def image_to_feature(file_name):
    try:
        im = Image.open(file_name)
        im.mode = 'I'
        im.thumbnail(im.size)
        return _bytes_feature(im.point(lambda i: i * (1. / 256)).convert('L').tobytes())
    except Exception as e:
        print('Exception', e)

# mag = '200X'
target_image_type = "Phase Contrast"
mag = 10
days = [1]

script_dir  = os.path.dirname(os.path.realpath(__file__))

base_tfrecord = os.path.join(script_dir, 'new', "images_experiment_split.tfrecords")

root_dst_dir = os.path.join(script_dir, 'new', 'experiment_split_tissue_type_split_tfrecord_{}_{}x_{}'.format(target_image_type, mag, '-'.join(str(day) for day in days)))

if os.path.exists(root_dst_dir):
    raise Exception("Destination path already exits")
else:
    os.makedirs(root_dst_dir)

record_iterator = tf.python_io.tf_record_iterator(path=base_tfrecord)

shards = 10

train_writers = []
test_writers = []
for i in range(1, 1 + shards):
    train_writers.append(tf.python_io.TFRecordWriter(os.path.join(root_dst_dir, "train-{}.tfrecords".format(i))))
    test_writers.append(tf.python_io.TFRecordWriter(os.path.join(root_dst_dir, "test-{}.tfrecords".format(i))))

train_writers_iter = itertools.cycle(train_writers)
test_writers_iter = itertools.cycle(test_writers)

num_train = defaultdict(int)
num_test = defaultdict(int)
for string_record in record_iterator:
    example = tf.train.Example()
    example.ParseFromString(string_record)
    image_type = example.features.feature['image_type'].bytes_list.value[0].decode()
    tissue_type = example.features.feature['tissue_type'].bytes_list.value[0].decode()
    day = example.features.feature['day'].int64_list.value[0]
    mag_objective = example.features.feature['mag_objective'].int64_list.value[0]
    train = example.features.feature['train'].int64_list.value[0]

    # print(day, image_type, mag_objective, tissue_type)
    if not day in days or mag != mag_objective or image_type != target_image_type:
        continue

    if train:
        writer = next(train_writers_iter)
        num_train[tissue_type] += 1
    else:
        writer = next(test_writers_iter)
        num_test[tissue_type] += 1
    writer.write(string_record)

    print(day, image_type, mag_objective, tissue_type)

print('num_train', num_train)
print('num_test', num_test)

for writer in train_writers + test_writers:
    writer.close()