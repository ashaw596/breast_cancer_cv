import os
import shutil
import numpy as np
import random
import tensorflow as tf
import parse

import os
from PIL import Image
from io import BytesIO


def dict_to_example(dictionary: dict):
    new_dict = {}
    for key, value in dictionary.items():
        if isinstance(value, str):
            value = _bytes_feature(value.encode())
        elif isinstance(value, int):
            value = _int64_feature(value)
        elif isinstance(value, bytes):
            value = _bytes_feature(value)
        elif not isinstance(value, tf.train.Feature):
            print(key, value)
            raise "Unrecognized Feature type"
        new_dict[key] = value
    return tf.train.Example(features=tf.train.Features(feature=new_dict))

def _int64_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def image_to_feature(file_name):
    try:
        im = Image.open(file_name)
        im.mode = 'I'
        im.thumbnail(im.size)
        # bytes_io = BytesIO()
        # with BytesIO() as f:
        #     im.point(lambda i: i * (1. / 256)).convert('L').save(f, 'png')
        #     return _bytes_feature(f.getvalue())

        return _bytes_feature(tf.compat.as_bytes(im.point(lambda i: i * (1. / 256)).convert('L').tobytes()))
    except Exception as e:
        print('Exception', e)

def to_example(root_src_dir, dir, file, train_test):
    dir_parsed = parse.parse("{start_date}_{start_time}_{experiment_name}", dir)

    # TODO: fix this. This won't always be valid
    if 'GMP' in dir_parsed['experiment_name']:
        tissue_type = 'cord_tissue'
    elif 'RB' in dir:
        tissue_type = 'bone_marrow'
    else:
        raise Exception("You did something wrong")

    cur_path = os.path.join(root_src_dir, dir, file)
    parsed = parse.parse("{well_id}_{mag_objective:d}_{unused}_{well_location:d}_{image_type}_{day:d}.tif", file)

    features = dict(parsed.named)
    features.pop('unused')
    features['file_name'] = file

    print(features)
    if features['mag_objective'] == 2:
        features['mag_objective'] = 4
    elif features['mag_objective'] == 3:
        features['mag_objective'] = 10
    else:
        raise "unrecognized objective"

    features['dir_name'] = dir
    features['experiment_start_date_str'] = dir_parsed['start_date']
    features['experiment_start_time_str'] = dir_parsed['start_time']
    features['experiment_name'] = dir_parsed['experiment_name']

    if features['experiment_name'] in ['RB180', 'GMP087'] :
        features['train'] = 0
        print('test')
    else:
        features['train'] = 1
        print('train')
    # if train_test == "train":
    #     features['train'] = 1
    # elif train_test == "test":
    #     features['train'] = 0
    # else:
    #     raise "Unrecognized train_test"

    img_raw = image_to_feature(os.path.join(cur_path))
    features['image_raw'] = img_raw
    features['tissue_type'] = tissue_type
    example = dict_to_example(features)
    return example


# mag = '200X'

script_dir  = os.path.dirname(os.path.realpath(__file__))
# print(script_dir)
root_src_dir = os.path.join(script_dir, 'org_data')

dest_tfrecord_filename = os.path.join(script_dir, 'new', 'images_experiment_split.tfrecords')

test_percent = 0.3
dirs = os.listdir(root_src_dir)
cords = []
bones = []
for dir in dirs:
    if 'GMP' in dir:
        cords.append(dir)
    elif 'RB' in dir:
        bones.append(dir)

random.shuffle(cords)
random.shuffle(bones)

train_cords_size = int(len(cords)*(1.0-test_percent))
train_cords = cords[:train_cords_size]
test_cords = cords[train_cords_size:]

train_bone_size = int(len(bones)*(1.0 - test_percent))
train_bones = bones[:train_bone_size]
test_bones = bones[train_bone_size:]

train = train_cords + train_bones
test = test_cords + test_bones

train = list(zip(train, ['train']*len(train)))
test = list(zip(test, ['test']*len(test)))

all = train + test

all_info = []
for dir, train_test in all:
    cur_path = os.path.join(root_src_dir, dir)
    for file in (name for name in os.listdir(cur_path) if name.endswith('.tif')):
        all_info.append((root_src_dir, dir, file, train_test))

random.shuffle(all_info)

with tf.python_io.TFRecordWriter(dest_tfrecord_filename) as writer:
    for root_src_dir, dir, file, train_test in all_info:
        example = to_example(root_src_dir, dir, file, train_test)
        writer.write(example.SerializeToString())