import os
import shutil
import numpy as np
import random

# mag = '200X'

script_dir  = os.path.dirname(os.path.realpath(__file__))
# print(script_dir)
root_src_dir = os.path.join(script_dir, 'org_data')

root_dst_dir = os.path.join(script_dir, 'tissue_type_split')

if os.path.exists(root_dst_dir):
    raise Exception("Destination path already exits")


# test_percent = 0.2

cords = []
bones = []

for dir in os.listdir(root_src_dir):
    cur_path = os.path.join(root_src_dir, dir)
    #TODO: fix this. This won't always be valid
    if 'GMP' in dir:
        dir_type = 'cord_tissue'
    elif 'RB' in dir:
        dir_type = 'bone_marrow'
    else:
        raise Exception("You did something wrong")

    for file in (name for name in os.listdir(cur_path) if name.endswith('.tif')):
        cur_path = os.path.join(root_src_dir, dir, file)

        dest_file_name = f"{dir}___{file}"
        if dir_type == "cord_tissue":
            cords.append((cur_path, dest_file_name))
        elif dir_type == "bone_marrow":
            bones.append((cur_path, dest_file_name))

cords_dest = os.path.join(root_dst_dir, 'cord_tissue')
bones_dest = os.path.join(root_dst_dir, 'bones_dest')

os.makedirs(cords_dest)
os.makedirs(bones_dest)

for path, name in cords:
    shutil.copy(path, os.path.join(cords_dest, name))


for path, name in bones:
    shutil.copy(path, os.path.join(bones_dest, name))

exit()



for type_name in ['benign', 'malignant']:
    root_src_dir = os.path.join(mag, type_name)
    # dest_dir = os.path.join(root_dst_dir, type_name)


    files = os.listdir(root_src_dir)
    random.shuffle(files)

    num_files = len(files)

    test_files = files[:int(test_percent*num_files)]
    train_files = files[int(test_percent*num_files):]

    train_dest_dir = os.path.join(root_dst_dir, 'train', type_name)
    test_dest_dir = os.path.join(root_dst_dir, 'test', type_name)
    if not os.path.exists(train_dest_dir):
        os.makedirs(train_dest_dir)

    if not os.path.exists(test_dest_dir):
        os.makedirs(test_dest_dir)

    for file_name in train_files:
        shutil.copy(os.path.join(root_src_dir, file_name), train_dest_dir)

    for file_name in test_files:
        shutil.copy(os.path.join(root_src_dir, file_name), test_dest_dir)
    # print(len(files))

    # if not os.path.exists(root_dst_dir):
    #     os.makedirs(root_dst_dir)


#
#
# for folder in os.listdir(root_src_dir):
#     print(folder)
#     subdir = os.path.join(root_src_dir, folder)
#     assert(not os.path.isfile(subdir))
#     for dir in os.listdir(subdir):
#         path = os.path.join(subdir, dir)
#         print(path)
#         for mag in os.listdir(path):
#             mag_path = os.path.join(path, mag)
#             print(mag_path)
#             for image in os.listdir(mag_path):
#                 image_path = os.path.join(mag_path, image)
#                 dest_path = os.path.join(root_dst_dir, mag)
#                 if not os.path.exists(dest_path):
#                     os.makedirs(dest_path)
#                 assert(not os.path.isfile(os.path.join(dest_path, image)))
#                 shutil.move(image_path, dest_path)
#
#             assert (not os.listdir(mag_path))
#             os.rmdir(mag_path)
#             # exit(1)
