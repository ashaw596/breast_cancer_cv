from tensorflow.python.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
from tensorflow.python.keras.models import Sequential, Model
from tensorflow.python.keras.layers import Dense, Dropout, Activation, Flatten, GlobalAveragePooling2D
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras.models import load_model
from scipy import ndimage
import os

os.getcwd()
import glob
import scipy
import math
import tensorflow as tf
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.applications.vgg16 import VGG16

image_size = [904, 1224, 1]

batch_size = 32*4
splits = 8
train_splits = 7
train_large_size = [image_size[0]//splits*(train_splits), image_size[1]//splits*(train_splits), image_size[2]]


def test_parse_fn(example):
  "Parse TFExample records and perform simple data augmentation."
  example_fmt = {
    "image_raw": tf.FixedLenFeature((), tf.string, ""),
    "mag_objective": tf.FixedLenFeature((), tf.int64, 0),
    "file_name": tf.FixedLenFeature((), tf.string, ""),
    "well_id": tf.FixedLenFeature((), tf.string, ""),
    "well_location": tf.FixedLenFeature((), tf.int64, 0),
    "image_type": tf.FixedLenFeature((), tf.string, ""),
    "day": tf.FixedLenFeature((), tf.int64, 0),
    "train": tf.FixedLenFeature((), tf.int64, 0),
    "dir_name": tf.FixedLenFeature((), tf.string, ""),
    "experiment_start_date_str": tf.FixedLenFeature((), tf.string, ""),
    "experiment_start_time_str": tf.FixedLenFeature((), tf.string, ""),
    "experiment_name": tf.FixedLenFeature((), tf.string, ""),
    "tissue_type": tf.FixedLenFeature((), tf.string, "")
  }
  parsed = tf.parse_single_example(example, example_fmt)
  image = tf.decode_raw(parsed["image_raw"], tf.uint8)
  image = tf.reshape(image, image_size)
  label = tf.equal(parsed["tissue_type"], b"cord_tissue")
  image = tf.image.convert_image_dtype(image, dtype=tf.float32)
  return image, label
def parse_fn(example):
  "Parse TFExample records and perform simple data augmentation."
  example_fmt = {
    "image_raw": tf.FixedLenFeature((), tf.string, ""),
    "mag_objective": tf.FixedLenFeature((), tf.int64, 0),
    "file_name": tf.FixedLenFeature((), tf.string, ""),
    "well_id": tf.FixedLenFeature((), tf.string, ""),
    "well_location": tf.FixedLenFeature((), tf.int64, 0),
    "image_type": tf.FixedLenFeature((), tf.string, ""),
    "day": tf.FixedLenFeature((), tf.int64, 0),
    "train": tf.FixedLenFeature((), tf.int64, 0),
    "dir_name": tf.FixedLenFeature((), tf.string, ""),
    "experiment_start_date_str": tf.FixedLenFeature((), tf.string, ""),
    "experiment_start_time_str": tf.FixedLenFeature((), tf.string, ""),
    "experiment_name": tf.FixedLenFeature((), tf.string, ""),
    "tissue_type": tf.FixedLenFeature((), tf.string, "")
  }
  parsed = tf.parse_single_example(example, example_fmt)
  image = tf.decode_raw(parsed["image_raw"], tf.uint8)
  image = tf.reshape(image, image_size)
  image = tf.random_crop(image, [image_size[0]//splits*(train_splits), image_size[1]//splits*(train_splits), image_size[2]])
  label = tf.equal(parsed["tissue_type"], b"cord_tissue")
  image = tf.image.convert_image_dtype(image, dtype=tf.float32)
  return image, label

def extract_image_patches_fn(splits, height, width, channels):
  def temp(images, label):
    images = tf.expand_dims(images, axis=0)
    sizes = [1, height//splits, width//splits, channels]
    patches = tf.extract_image_patches(images, ksizes=sizes, strides=[1, height//splits, width//splits, 1], rates=[1,1,1,1], padding="VALID")
    labels = tf.tile(tf.expand_dims(label, 0), [splits*splits])
    patches = tf.reshape(patches, [splits*splits, sizes[1], sizes[2],sizes[3]])
    # patches = image_to_patches(images, height, width, height//splits, width//splits)
    print(patches, labels)
    labels = tf.cast(labels, tf.int32)
    labels = tf.one_hot(labels, 2)
    patches = tf.image.grayscale_to_rgb(patches)
    return tf.data.Dataset.from_tensor_slices((patches, labels))
  return temp
folder_name = '''experiment_split_tissue_type_split_tfrecord_Phase Contrast_10x_1'''

data_dir = os.path.join(os.getcwd(), 'data', '180328_121316_6WP MSC Imaging', 'new', folder_name)

train_tfrecords = os.path.join(data_dir, "train-*.tfrecords")
test_tfrecords = os.path.join(data_dir, "test-*.tfrecords")
# for name in glob.glob(train_tfrecords):
#     print(name)
# print(train_tfrecords)
def test_input_fn():
  test_tfrecords_list = tf.data.Dataset.list_files(test_tfrecords)
  test_dataset = test_tfrecords_list.shuffle(10).interleave(tf.data.TFRecordDataset, cycle_length=1, block_length=1)
  # train_dataset = tf.data.TFRecordDataset(train_tfrecords_list)
  test_dataset = test_dataset.shuffle(300).map(map_func=test_parse_fn)
  test_dataset = test_dataset.flat_map(extract_image_patches_fn(splits, image_size[0], image_size[1], image_size[2]))
  test_dataset = test_dataset.shuffle(splits*splits*100).map(map_func= lambda x,y: ({'input_1': x}, y))
  test_dataset = test_dataset.batch(batch_size).repeat()
  # return train_dataset
  iterator  = test_dataset.make_one_shot_iterator()
  return iterator.get_next()

def train_input_fn():
  train_tfrecords_list = tf.data.Dataset.list_files(train_tfrecords)
  train_dataset = train_tfrecords_list.shuffle(10).interleave(tf.data.TFRecordDataset, cycle_length=1, block_length=1)
  # train_dataset = tf.data.TFRecordDataset(train_tfrecords_list)
  train_dataset = train_dataset.shuffle(300).map(map_func=parse_fn)
  train_dataset = train_dataset.flat_map(extract_image_patches_fn(train_splits,train_large_size[0], train_large_size[1], train_large_size[2]))
  train_dataset = train_dataset.shuffle(train_splits*train_splits*100).map(map_func= lambda x,y: ({'input_1': x}, y))
  train_dataset = train_dataset.batch(batch_size).repeat()
  # return train_dataset
  iterator  = train_dataset.make_one_shot_iterator()
  return iterator.get_next()
# next_element = iterator.get_next()
# data, label = input_fn()
# with tf.Session() as sess:
#   for i in range(100000):
#     value = sess.run([data, label])
#     print(value[0])
#     print(value[1])
#     print(i)
    # print(value)


save_dir = os.path.join(os.getcwd(), folder_name + "_save_1")
print(save_dir)

if os.path.exists(save_dir):
    raise Exception("Destination path already exits")
else:
    os.makedirs(save_dir)

# base_model = VGG16(weights='imagenet', include_top=False)
base_model = VGG16(weights='imagenet', include_top=False, input_shape=(226//2, 306//2, 3))
# base_model = VGG16(weights=None, include_top=False, input_shape=(226, 306, 3))
# base_model = keras.applications.xception.Xception(weights='imagenet', include_top=False, input_shape=(226, 306, 3))

# add a global spatial average pooling layer
x = base_model.output
x = Flatten()(x)
# x = GlobalAveragePooling2D()(x)
# let's add a fully-connected layer
x = Dense(512, activation='relu')(x)
x = Dropout(0.85)(x)
# and a logistic layer -- let's say we have 200 classes
predictions = Dense(2, activation='softmax')(x)


# this is the model we will train
model = Model(inputs=base_model.input, outputs=predictions)
print(model.input_names)

reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.5,
                              patience=5, min_lr=0.000005)
model.compile(loss='categorical_crossentropy',
              optimizer=Adam(lr=0.00001),
              # optimizer=keras.optimizers.RMSprop(lr=0.00001, decay = 0.0005),
              metrics=['accuracy'])
estimator = tf.keras.estimator.model_to_estimator(keras_model=model, model_dir=save_dir)
train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn, max_steps=3000)
eval_spec = tf.estimator.EvalSpec(input_fn=test_input_fn)
print(tf.estimator.train_and_evaluate(
    estimator,
    train_spec,
    eval_spec
))