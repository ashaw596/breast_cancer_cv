import os
import random

import tensorflow as tf
from tensorflow.python.framework.errors_impl import OutOfRangeError

from tensorflow.python.keras.callbacks import ReduceLROnPlateau
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
from tensorflow.python.keras.models import Sequential, Model
from tensorflow.python.keras.layers import Dense, Dropout, Activation, Flatten, GlobalAveragePooling2D
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.applications.vgg16 import VGG16
from PIL import Image, ImageFilter


import numpy as np

import matplotlib.pyplot as plt













def save_image(array: np.array, filename):
  if array.dtype == np.float32:
    array *= 255
    array = array.astype(np.uint8)

  im = Image.fromarray(np.squeeze(array))
  if im.mode != 'RGB':
    im = im.convert('RGB')
  im.save(filename, quality=100)

image_size = [904, 1224, 1]
splits = 8

def parse_fn(example):
  "Parse TFExample records and perform simple data augmentation."
  example_fmt = {
    "image_raw": tf.FixedLenFeature((), tf.string, ""),
    "mag_objective": tf.FixedLenFeature((), tf.int64, 0),
    "file_name": tf.FixedLenFeature((), tf.string, ""),
    "well_id": tf.FixedLenFeature((), tf.string, ""),
    "well_location": tf.FixedLenFeature((), tf.int64, 0),
    "image_type": tf.FixedLenFeature((), tf.string, ""),
    "day": tf.FixedLenFeature((), tf.int64, 0),
    "train": tf.FixedLenFeature((), tf.int64, 0),
    "dir_name": tf.FixedLenFeature((), tf.string, ""),
    "experiment_start_date_str": tf.FixedLenFeature((), tf.string, ""),
    "experiment_start_time_str": tf.FixedLenFeature((), tf.string, ""),
    "experiment_name": tf.FixedLenFeature((), tf.string, ""),
    "tissue_type": tf.FixedLenFeature((), tf.string, "")
  }
  parsed = tf.parse_single_example(example, example_fmt)
  image = tf.decode_raw(parsed["image_raw"], tf.uint8)
  image = tf.reshape(image, image_size)
  label = tf.equal(parsed["tissue_type"], b"cord_tissue")
  image = tf.image.convert_image_dtype(image, dtype=tf.float32)

  return image, label, parsed


def extract_image_patches_fn(splits, height, width, channels):
  def temp(images, label):
    images = tf.expand_dims(images, axis=0)
    sizes = [1, height//splits, width//splits, channels]
    patches = tf.extract_image_patches(images, ksizes=sizes, strides=[1, height//splits, width//splits, 1], rates=[1,1,1,1], padding="VALID")
    labels = tf.tile(tf.expand_dims(label, 0), [splits*splits])
    patches = tf.reshape(patches, [splits*splits, sizes[1], sizes[2],sizes[3]])
    # patches = image_to_patches(images, height, width, height//splits, width//splits)
    print(patches, labels)
    labels = tf.cast(labels, tf.int32)
    labels = tf.one_hot(labels, 2)
    patches = tf.image.grayscale_to_rgb(patches)
    return (patches, labels)
  return temp

folder_name = '''experiment_split_tissue_type_split_tfrecord_Phase Contrast_10x_1-2'''
save_model_folder = os.path.join(os.getcwd(), folder_name + '_save_1')

data_dir = os.path.join(os.getcwd(), 'data', '180328_121316_6WP MSC Imaging', 'new', folder_name)
save_viz_dir = os.path.join(os.getcwd(), 'viz', folder_name + '_count_1')

if save_viz_dir:
  os.makedirs(save_viz_dir)
test_tfrecords = os.path.join(data_dir, "test-*.tfrecords")
# for name in glob.glob(train_tfrecords):
#     print(name)
# print(train_tfrecords)
def test_input_fn():
  test_tfrecords_list = tf.data.Dataset.list_files(test_tfrecords)
  test_dataset = test_tfrecords_list.interleave(tf.data.TFRecordDataset, cycle_length=1, block_length=1)
  test_dataset = test_dataset.map(map_func=parse_fn)
  test_dataset = test_dataset.map(lambda image, label, parsed: extract_image_patches_fn(splits, image_size[0], image_size[1], image_size[2])(image, label) + (image, label, parsed))
  test_dataset = test_dataset.map(map_func= lambda patches, labels, image, label, parsed: ((patches, labels), (image, label, parsed)))
  iterator  = test_dataset.make_one_shot_iterator()
  input, other = iterator.get_next()
  return input, other

def softmax(x, axis=-1):
  """Compute softmax values for each sets of scores in x."""
  e_x = np.exp(x - np.max(x, axis=axis, keepdims=True))
  return e_x/np.sum(e_x, axis=axis, keepdims=True)

base_model = VGG16(weights='imagenet', include_top=False, input_shape=(226//2, 306//2, 3))
x = base_model.output
x = Flatten()(x)
x = Dense(512, activation='relu')(x)
x = Dropout(0.85)(x)
predictions = Dense(2, activation='softmax')(x)
model = Model(inputs=base_model.input, outputs=predictions)
print(model.input_names)

reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.5,
                              patience=5, min_lr=0.000005)
model.compile(loss='categorical_crossentropy',
              optimizer=Adam(lr=0.00001),
              # optimizer=keras.optimizers.RMSprop(lr=0.00001, decay = 0.0005),
              metrics=['accuracy'])
estimator = tf.keras.estimator.model_to_estimator(keras_model=model, model_dir=save_model_folder)

(patches_tf, labels_tf), (image_tf, label_tf, parsed_tf) = test_input_fn()
#for i in range(100):
#  print(estimator.evaluate(input_fn=test_input_fn, steps=1))


data = []
with tf.Session() as sess:
    for i in range(10000):
        try:
          patches, lab, parsed, full_image = sess.run([patches_tf, labels_tf, parsed_tf, image_tf])
          # patches = patches['input_1']

          for key in parsed:
              if key == 'image_raw':
                  continue
              if isinstance(parsed[key], bytes):
                  parsed[key] = parsed[key].decode()
          file_name = "{}__tissueType-{}_day-{}_mag-{}_wellId-{}_wellLoc-{}_imageType-{}"\
              .format(parsed['experiment_name'], parsed['tissue_type'], parsed['day'], parsed['mag_objective'],
                      parsed['well_id'], parsed['well_location'], parsed['image_type'])

          data.append(((patches, lab), (file_name, full_image)))
          # print(lab)
          print(parsed['tissue_type'])
          print(i)
        except OutOfRangeError:
          print("OutOfRangeError")
          break
num_correct = 0
total = 0
accs=[]
for ((patches, lab), (file_name, full_image)) in data:
  def input_fn():
    return tf.data.Dataset.from_tensor_slices(tf.convert_to_tensor(patches)).map(lambda x: {'input_1': x}).batch(10000).make_one_shot_iterator().get_next()

  # input_fn.run = False
  # print(patches)
  output = [output['dense_2'] for output in estimator.predict(input_fn=input_fn)]
  output = np.array(output)
  # print(output)
  probs = softmax(output, axis=1)
  # total = np.mean(probs, axis=0)
  predicted = np.argmax(output, axis=1)
  correct_index = np.argmax(lab[0])
  correct_patches = predicted == correct_index
  acc = np.mean(correct_patches)
  print(acc)
  if acc > 0.5:
    num_correct+=1
  if save_viz_dir:
    save_image(full_image, os.path.join(save_viz_dir, 'full_{}_{}.jpg'.format(acc, file_name)))
    for i in range(probs.shape[0]):
      save_image(patches[i], os.path.join(save_viz_dir, 'part_{}_{}_{:03d}.jpg'.format(probs[i][correct_index], file_name, i)))
  total +=1
  accs.append(acc)
  print(num_correct/total, lab[0])

print(np.mean(accs))
print(num_correct, total)

exit()
# num_correct = 0
# total = batch_size
#
# for input, label in zip(X, Y):
#
#     patches = []
#     for i in range(split):
#         x = i*patch_width
#         for j in range(split):
#             y = j*patch_height
#             im = input[y:y+patch_height,x:x+patch_width, :]
#             patches.append(im)
#     output = model.predict(np.array(patches))
#     s = np.sum(output, axis=0)
#
#     # print(np.argmax(s))
#     # print(np.argmax(label))
#     correct = np.argmax(s) == np.argmax(label)
#
#     print('label', label)
#     print('s', s)
#     print('correct', correct)
#     num_correct += correct
#
# print('num_correct', num_correct)
# print('num_correct/total', num_correct/total)

num_correct = 0
total = 0

num_patches = 200

for input, label in zip(X, Y):


    patches = []
    for i in range(num_patches):
        x = random.randrange(0, width-patch_width)
        y = random.randrange(0, height-patch_height)
        im = input[y:y+patch_height,x:x+patch_width, :]

        patches.append(im)

    img = np.array(patches)

    plt.imshow(img[0])
    print(img[0].shape)
    plt.show()
    output = model.evaluate(img, np.ones((200,1))*np.reshape(label,(1,2)))
    print(output)
    # s = output[1] > 0.5

    # print(np.argmax(s))
    # print(np.argmax(label))
    correct_patches = output[1] > 0.5

    print('label', label)
    # print('s', s)
    print('correct', correct_patches)
    num_correct += correct_patches
    total += 1
    print('num_correct/total', num_correct/total)


print(num_patches, " random patches")
print('num_correct', num_correct)
print('num_correct/total', num_correct/total)